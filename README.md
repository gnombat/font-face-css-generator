# font-face-css-generator.py

This program generates @font-face CSS for a set of .woff2 files.

## Usage

```
./font-face-css-generator.py -o CSS_FILE.css FONT_FILE.woff2 ...
```

It is expected that the .woff2 files will follow a certain naming convention.
Each filename must match one of the following patterns (where *FAMILY* is the name of the font family):

* *FAMILY*-Thin.woff2
* *FAMILY*-ThinItalic.woff2
* *FAMILY*-ExtraLight.woff2
* *FAMILY*-ExtraLightItalic.woff2
* *FAMILY*-Light.woff2
* *FAMILY*-LightItalic.woff2
* *FAMILY*-Regular.woff2
* *FAMILY*-Italic.woff2
* *FAMILY*-Medium.woff2
* *FAMILY*-MediumItalic.woff2
* *FAMILY*-SemiBold.woff2
* *FAMILY*-SemiBoldItalic.woff2
* *FAMILY*-Bold.woff2
* *FAMILY*-BoldItalic.woff2
* *FAMILY*-ExtraBold.woff2
* *FAMILY*-ExtraBoldItalic.woff2
* *FAMILY*-Black.woff2
* *FAMILY*-BlackItalic.woff2

This naming convention is used by Google Fonts.
(But note that Google Fonts provides downloadable fonts in the form of .ttf files instead of .woff2 files.
You can convert .ttf files to .woff2 files using the [`woff2_compress` program](https://github.com/google/woff2).)

Variable fonts are not currently supported.
A future version of this program may add support for variable fonts.

## Example

Download [Noto Sans from Google Fonts](https://fonts.google.com/noto/specimen/Noto+Sans).

```
unzip Noto_Sans.zip -d Noto_Sans
find Noto_Sans/static -name '*.ttf' -exec woff2_compress {} \;
./font-face-css-generator.py -o fonts.css Noto_Sans/static/*.woff2
```

## License

This program is free software. See the file "LICENSE.txt" for license terms.
