#!/usr/bin/env python3

import argparse
import os
import re
import sys

class FontFile:
  pass

def main():
  parser = argparse.ArgumentParser(description = 'Generate @font-face CSS for WOFF2 font files.')
  parser.add_argument('WOFF2_FILE', nargs = '+', help = 'the .woff2 font file(s)')
  parser.add_argument('-o', '--output', metavar = 'CSS_FILE', help = 'the output .css file (default: standard output)')
  args = parser.parse_args()

  regex = re.compile(r'(.+)-(Thin|ExtraLight|Light|Regular|Medium|SemiBold|Bold|ExtraBold|Black)?(Italic)?')
  weight_map = {
    'Thin': 100,
    'ExtraLight': 200,
    'Light': 300,
    'Regular': 400,
    'Medium': 500,
    'SemiBold': 600,
    'Bold': 700,
    'ExtraBold': 800,
    'Black': 900,
  }

  font_files = []
  for woff2_file in args.WOFF2_FILE:
    basename = os.path.basename(woff2_file)
    (font_name, extension) =  os.path.splitext(basename)
    if extension == '':
      sys.stderr.write('File has no extension: ' + woff2_file + '\n')
      continue

    if extension != '.woff2':
      sys.stderr.write('File does not have .woff2 extension: ' + woff2_file + '\n')
      continue

    match = regex.fullmatch(font_name)
    if match is None:
      sys.stderr.write('Could not parse font name: ' + font_name + '\n')
      continue

    font_family = match.group(1)
    font_weight = match.group(2)
    font_style = match.group(3)

    font_file = FontFile()

    font_file.filename = woff2_file
    font_file.font_family = font_family
    font_file.font_format = 'woff2'

    if font_weight is None:
      font_file.font_weight = 400
    else:
      font_file.font_weight = weight_map[font_weight]

    if font_style is None:
      font_file.is_italic = 0
    else:
      font_file.is_italic = 1

    font_files.append(font_file)

  # sort the fonts
  font_files.sort(key = lambda font_file: (font_file.font_family, font_file.font_weight, font_file.is_italic))

  # output the CSS
  if args.output:
    output_file = args.output
  else:
    output_file = '-'

  if output_file == '-':
    output_directory = os.getcwd()
    write_css(font_files, sys.stdout, output_directory)
  else:
    output_directory = os.path.dirname(output_file)
    with open(output_file, 'w', encoding = 'UTF-8') as f:
      write_css(font_files, f, output_directory)

def write_css(font_files, f, output_directory):
  for font_file in font_files:
    if font_file.is_italic:
      font_style = 'italic'
    else:
      font_style = 'normal'

    url = os.path.relpath(font_file.filename, output_directory)

    f.write('@font-face {\n')
    f.write('\tfont-family: \'' + font_file.font_family + '\';\n')
    f.write('\tfont-weight: ' + str(font_file.font_weight) + ';\n')
    f.write('\tfont-style: ' + font_style + ';\n')
    f.write('\tsrc: url(\'' + url + '\') format(\'' + font_file.font_format + '\');\n')
    f.write('}\n')

if __name__ == '__main__':
  main()
